import asciidoctor from '@asciidoctor/core'

const Asciidoctor = asciidoctor()

function toDocumentObjectModel (doc, text) {
  const blocks = doc.getBlocks().map((block) => toNode(block))
  const attributes = doc.attributes_modified.$to_a().reduce((accum, attr) => {
    accum[attr] = doc.getAttribute(attr)
    return accum
  }, {})
  return {
    name: 'document',
    type: 'block',
    ...(doc.hasHeader() && {
      attributes,
      header: {
        title: {
          inlines: [{ name: 'text', type: 'string', value: doc.getDocumentTitle() }],
        },
      },
    }),
    blocks,
  }
}

function toNode (block) {
  const name = block.getNodeName()
  const type = 'block'
  if (block.$content_model() === 'simple') {
    const attributes = {}
    const id = block.getId()
    if (id) attributes.id = id
    const subs = block.subs.filter((sub) => sub !== 'replacements')
    return {
      name,
      type,
      ...(Object.keys(attributes).length > 0 && { attributes }),
      inlines: toInlines(block.lines.map((l) => block.applySubstitutions(l, subs)).join(' ')),
    }
  }
  if (name === 'paragraph') {
    const subs = block.subs.filter((sub) => sub !== 'replacements')
    return {
      name,
      type,
      inlines: [block.lines.map((l) => block.applySubstitutions(l, subs)).join(' ')],
    }
  }
  if (block.getNodeName() === 'section') {
    return {
      name,
      type,
      title: {
        inlines: [{ name: 'text', type: 'string', value: block.getTitle() }],
      },
      level: block.getLevel(),
      blocks: block.getBlocks().map((block) => toNode(block)),
    }
  }
  if (block.getNodeName() === 'listing' || block.getNodeName() === 'literal') {
    const attributes = block.getAttributes()
    const style = block.getStyle()
    if (style === block.getNodeName()) delete attributes.style
    //const title = block.getTitle()
    delete attributes.$positional
    return {
      name,
      type,
      ...(Object.keys(attributes).length > 0 && { attributes }),
      inlines: [
        {
          name: 'text',
          type: 'string',
          value: block.lines.join('\n'),
        },
      ],
    }
  }
  if (block.getNodeName() === 'list_item') {
    const blocks = block.getBlocks()
    return {
      name: 'listItem',
      type,
      marker: block.getMarker(),
      principal: {
        inlines: [{ name: 'text', type: 'string', value: block.getText() }],
      },
      blocks: blocks.map((block) => toNode(block)),
    }
  }
  if (block.getNodeName() === 'ulist') {
    return {
      name: 'list',
      type,
      marker: block.getItems()[0].getMarker(),
      variant: 'unordered',
      items: block.getBlocks().map((block) => toNode(block)),
    }
  }
  const attributes = {}
  const id = block.getId()
  if (id) attributes.id = id
  return {
    type,
    ...(Object.keys(attributes).length > 0 && { attributes }),
    blocks: block.getBlocks().map((block) => toNode(block)),
  }
}

function toInlines (str) {
  if (str.startsWith('<strong>') && str.endsWith('</strong>')) {
    return [
      { name: 'span', type: 'inline', variant: 'strong', form: 'constrained', inlines: toInlines(str.slice(8, -9)) },
    ]
  }
  return [{ name: 'text', type: 'string', value: str }]
}

export default class AsciidoctorParser {
  parse (text) {
    const doc = Asciidoctor.load(text, { safe: 'safe' })
    // fixme: transform doc into a generic DOM
    const result = toDocumentObjectModel(doc, text)
    return result
  }
}
