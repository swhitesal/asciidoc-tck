/* eslint-env mocha */

import { expect } from 'chai'
import runner from '#runner'

describe('runner()', () => {
  it('should fail when using an non existing adapter', async () => {
    const noop = function () {}
    const result = await runner('non-existing-adapter', { reporter: noop })
    expect(result.failures).to.eq(6)
    expect(result.stats.failures).to.eq(result.failures)
    expect(result.stats.suites).to.eq(10)
    expect(result.stats.tests).to.eq(result.failures)
  })
})
