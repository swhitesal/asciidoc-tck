#!/usr/bin/env node

process.pkg
  ? require('../lib/index.cjs') // generated from index.js by Rollup
  : import('../lib/index.js')
