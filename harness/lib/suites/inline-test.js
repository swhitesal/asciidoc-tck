/* eslint-env mocha */
import { expect, makeTests, stringifyASG } from '../test-framework.js'

describe('inline', function () {
  const { parse, tests } = this.parent.ctx
  const inlineTests = tests.find((it) => it.type === 'dir' && it.name === 'inline').entries
  makeTests(inlineTests, async function ({ input, inputPath, expected, expectedWithoutLocations }) {
    const actualRoot = await parse(input)
    expect(actualRoot).to.have.property('blocks')
    expect(actualRoot.blocks).to.have.lengthOf(1)
    expect(actualRoot.blocks[0]).to.have.property('inlines')
    const actual = actualRoot.blocks[0].inlines
    if (expected == null) {
      // Q: can we write data to expected file automatically?
      // TODO only output expected if environment variable is set
      console.log(stringifyASG(actual))
      this.skip()
    } else {
      const msg = `actual output does not match expected output for ${inputPath}`
      // TODO wrap this in expectAsg helper
      expect(actual, msg).to.eql(!actual.length || 'location' in actual[0] ? expected : expectedWithoutLocations)
    }
  })
})
