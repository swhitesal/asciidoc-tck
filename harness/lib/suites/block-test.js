/* eslint-env mocha */
import { expect, makeTests, stringifyASG } from '../test-framework.js'

describe('block', function () {
  const { parse, tests } = this.parent.ctx
  const blockTests = tests.find((it) => it.type === 'dir' && it.name === 'block').entries
  makeTests(blockTests, async function ({ input, inputPath, expected, expectedWithoutLocations }) {
    const actual = await parse(input)
    if (expected == null) {
      // Q: can we write data to expect file automatically?
      // TODO only output expected if environment variable is set
      console.log(stringifyASG(actual))
      this.skip()
    } else {
      const msg = `actual output does not match expected output for ${inputPath}`
      // TODO wrap this in expectAsg helper
      expect(actual, msg).to.eql('location' in actual ? expected : expectedWithoutLocations)
    }
  })
})
