import run from './runner.js'

const adapter = process.env.ASCIIDOC_TCK_ADAPTER

if (adapter) {
  ;(async () =>
    run(adapter).then(
      (result) => process.exit(result.failures),
      (err) => console.error('Something went wrong!', err) || process.exit(1)
    ))()
} else {
  console.error('No tests run.')
  console.error('You must specify the path to a TCK adapter using the ASCIIDOC_TCK_ADAPTER environment variable.')
  process.exit(1)
}
