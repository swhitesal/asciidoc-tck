import { exec } from 'node:child_process'

export default class AdapterManager {
  constructor (adapter) {
    this.adapter = adapter
  }

  post (data) {
    return new Promise((resolve, reject) => {
      const cp = exec(this.adapter, (err, stdout, _stderr) => {
        if (err) return reject(err)
        resolve(JSON.parse(stdout))
      })
      cp.stdin.on('error', (err) => undefined).end(data) // eslint-disable-line node/handle-callback-err
    })
  }

  start () {}

  stop () {}
}
