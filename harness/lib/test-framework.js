export { expect } from 'chai'

export function makeTests (tests, testBlock) {
  /* eslint-env mocha */
  for (const test of tests) {
    const { name, type } = test
    if (type === 'dir') {
      describe(name, () => makeTests(test.entries, testBlock))
    } else {
      ;(it[test.condition] || it)(name, function () {
        return testBlock.call(this, test.data)
      })
    }
  }
}

export function stringifyASG (asg) {
  const locations = []
  return JSON.stringify(asg, (key, val) => (key === 'location' ? locations.push(val) - 1 : val), 2).replace(
    /("location": )(\d+)/g,
    (_, key, idx) => {
      return (
        key +
        JSON.stringify(locations[Number(idx)], null, 2)
          .replace(/\n */g, ' ')
          .replace(/(\[) | (\])/g, '$1$2')
      )
    }
  )
}

export { default } from 'mocha'
