import AdapterManager from './adapter-manager.js'
import loadTests from './test-loader.js'
import Mocha from './test-framework.js'
import ospath from 'node:path'
import { toModuleContext } from './helpers.js'

const PACKAGE_DIR = ospath.join(toModuleContext(import.meta).__dirname, '../..')

export default async (adapter, options = {}) => {
  const mocha = new Mocha({ timeout: 5000, ...options })
  // TODO start and stop adapter in global hooks
  const adapterManager = new AdapterManager(adapter)
  Object.assign(mocha.suite.ctx, {
    async parse (input) {
      return adapterManager.post(input)
    },
    tests: await loadTests(ospath.join(PACKAGE_DIR, 'tests')),
  })
  mocha.files = [
    ospath.join(PACKAGE_DIR, 'harness/lib/suites/block-test.js'),
    ospath.join(PACKAGE_DIR, 'harness/lib/suites/inline-test.js'),
  ]
  await mocha.loadFilesAsync()
  return new Promise((resolve) => {
    const runner = mocha.run((failures) => resolve({ failures, stats: runner.stats }))
  })
}
