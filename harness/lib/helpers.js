import { fileURLToPath } from 'node:url'
import ospath from 'node:path'

export function toModuleContext ({ url }) {
  const __filename = fileURLToPath(url)
  const __dirname = ospath.dirname(__filename)
  return { __dirname, __filename }
}
