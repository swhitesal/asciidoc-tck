export default {
  input: 'harness/lib/index.js',
  output: {
    file: 'harness/lib/index.cjs',
    format: 'cjs'
  },
  external: [
    "node:child_process",
    "node:fs/promises",
    "node:url",
    "node:path",
    "glob",
    "chai",
    "mocha"
  ]
}
